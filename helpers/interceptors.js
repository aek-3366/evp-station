import axios from "axios";
import { Base64 } from "js-base64";
import { getCookies, deleteCookie } from "cookies-next";

const apiFetch = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BASEURL,
});

apiFetch.interceptors.request.use(
  (request) => {
    const { token } = getCookies("token") || "";
    request.headers["x-access-token"] = token;
    request.headers["x-platform"] = "WEB";
    request.headers["Accept-Language"] = "TH";
    request.headers["Content-Type"] = "application/json";
    request.headers["x-access-login-application"] = "WEB";
    request.headers.Authorization = `Basic ${Base64.encode(
      process.env.NEXT_PUBLIC_BASICUSER +
        ":" +
        process.env.NEXT_PUBLIC_BASICPASS
    )}`;

    return request;
  },
  (error) => {
    return Promise.reject(error);
  }
);

apiFetch.interceptors.response.use(
  (response) => {
    if (
      response.data.res_code === "9003" ||
      response.data.res_code === "9004" ||
      response.data.res_code === "8051"
    ) {
      deleteCookie("token");
      deleteCookie("accInfo");
      setTimeout(() => {
        location.replace("/");
      }, 1500);
    }
    return response;
  },
  (error) => {
    //TODO
  }
);

export default apiFetch;
