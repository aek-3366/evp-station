import { createContext, useState, useEffect } from "react";
import { getCookies, getCookie, setCookie, deleteCookie } from "cookies-next";

const AuthContext = createContext({
  // auth: null,
  // setAuth: () => {},
  // user: null,
});

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState({});
  const [accInfo, setAccInfo] = useState({});
  const [idStation, setIdStation] = useState("");
  //   const [user, setUser] = useState(null);

  // useEffect(() => setAuth({token: getCookie('token') || null}), []);

  useEffect(() => {
    setAuth({ token: getCookie("token") || null });
    const cookiesInfo =
      getCookie("accInfo") !== undefined
        ? JSON.parse(getCookie("accInfo"))
        : null;
    const idStationCookies = getCookie("_id") !== undefined ? getCookie("_id") : null
    setAccInfo(cookiesInfo);
    setIdStation(idStationCookies);
  }, []);

  return (
    <AuthContext.Provider
      value={{ auth, setAuth, accInfo, setAccInfo, idStation, setIdStation }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
