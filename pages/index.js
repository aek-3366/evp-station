import Login from "@/components/login";
import useLoading from "@/hooks/useLoading";
import Loading from "@/components/loading";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { hasCookie } from "cookies-next";

export default function Home() {
  const { isLoading } = useLoading();
  const router = useRouter();
  const cookies = hasCookie("token");
  const _id = hasCookie("_id");

  useEffect(() => {
    if (cookies && _id) {
      router.push("/management/t?type=management-list");
    }
  }, [cookies, _id]);
  return (
    <>
      {isLoading ? <Loading /> : null}
      <Login />
    </>
  );
}
