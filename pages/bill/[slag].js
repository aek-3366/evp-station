import React from "react";
import BillHistory from "@/components/billHistory";
import BillRevenue from "@/components/billRevenue";
import { useRouter } from "next/router";
import useLoading from "@/hooks/useLoading";
import Loading from "@/components/loading";

export default function BillPage() {
  const router = useRouter();
  const { isLoading } = useLoading();

  return (
    <>
      {isLoading ? <Loading /> : null}
      {router.query.type === "bill-history" && <BillHistory />}
      {router.query.type === "bill-revenue" && <BillRevenue />}
    </>
  );
}
