import React from "react";
import ManagementList from "@/components/managementList";
import ManagementHistory from "@/components/managementHistory";
import ManagementInfo from "@/components/managementInfo";
import { useRouter } from "next/router";
import useLoading from "@/hooks/useLoading";
import Loading from "@/components/loading";

export default function ManagementPage() {
  const router = useRouter();
  const { isLoading } = useLoading();
  return (
    <>
      {isLoading ? <Loading /> : null}
      {router.query.type === "management-list" && <ManagementList />}
      {router.query.type === "management-history" && <ManagementHistory />}
      {router.query.type === "management-infomation" && <ManagementInfo />}
    </>
  );
}
