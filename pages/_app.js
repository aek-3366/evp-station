import "../styles/scss/globals.scss";
import { Box, ThemeProvider } from "@mui/material";
import theme from "../config/theme";
import Sidebar from "@/components/sidebar";
import Navbar from "@/components/navbar";
import { useRouter } from "next/router";
import { useMediaQuery } from "@material-ui/core";
import { useEffect } from "react";
import { LoadingProvider } from "@/context/LoadingProvider";
import { AuthProvider } from "@/context/AuthProvider";
import Head from "next/head";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const overMediumScreen = useMediaQuery(theme.breakpoints.up("md"));
  const lessMediumScreen = useMediaQuery(theme.breakpoints.down("md"));

  useEffect(() => {
    if (!router.isReady) {
      return;
    }
  }, [router]);

  return (
    <LoadingProvider>
      <AuthProvider>
        <ThemeProvider theme={theme}>
          <Head>
            <title>EVP Station</title>
          </Head>
          <Box className="containerFlex">
            {/* {overMediumScreen && router.asPath !== "/" && <Sidebar />} */}
            {overMediumScreen &&
              router.asPath !== "/" &&
              router.query.type !== "charging-control" &&
              router.query.type !== "news-promotion" &&
              router.query.type !== "station-info" &&
              router.query.type !== "setting-news-add" &&
              router.query.type !== "setting-news-edit" && <Sidebar />}
            {lessMediumScreen && router.asPath !== "/" && <Navbar />}
            <Component {...pageProps} />
          </Box>
        </ThemeProvider>
      </AuthProvider>
    </LoadingProvider>
  );
}

export default MyApp;
