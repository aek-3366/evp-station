import React, { useState, useEffect, useContext } from "react";
import styles from "./Login.module.scss";
import Image from "next/image";
import {
  Box,
  Container,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Typography,
  FormControl,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Button,
} from "@mui/material";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import EmailIcon from "@mui/icons-material/Email";
import ModalChooseStation from "@/components/modal/ModalChooseStation";
import ApiFetch from "@/helpers/interceptors";
import { errorAlert, warningAlert } from "@/utils/alert";
import AuthContext from "@/context/AuthProvider";
import useLoading from "@/hooks/useLoading";
import apiFetch from "@/helpers/interceptors";
import { setCookie, deleteCookie } from "cookies-next";

export default function Login() {
  const context = useContext(AuthContext);
  const { setIsLoading } = useLoading();
  const [showPassword, setShowPassword] = useState();
  const [values, setValues] = useState({
    email: "",
    password: "",
  });
  const [disableBtn, setDisableBtn] = useState(true);
  const [openModal, setOpenModal] = useState(false);
  const [station,setStation] = useState([])

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const login = async () => {
    // ถ้า login ผ่าน ถึงเรียกใช้ modal เลือกสถานี
    try {
      setIsLoading(true);
      const body = {
        Email: values.email,
        password: values.password,
      };
      const res = await apiFetch.post("/token/login/admin/station", body);
      if (res.data.res_code === "0000") {
        setCookie("token", res.data.res_data.token);
        const resInfo = await ApiFetch.get("/token/info/admin");
        const resStationName = await ApiFetch.get(
          "/stations/mylist/namestation"
        );
        if (
          resInfo.data.res_code === "0000" &&
          resStationName.data.res_code === "0000"
        ) {
          setStation(resStationName.data.res_data)
          setCookie("accInfo", resInfo.data.res_data);
          context.setAccInfo(resInfo.data.res_data);
          setIsLoading(false);
          setOpenModal(true);
        } else {
          deleteCookie("token");
          setIsLoading(false);
        }
      } else {
        deleteCookie("token");
        warningAlert(res.data.res_message);
        setIsLoading(false);
      }
    } catch (error) {
      errorAlert(error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (
      /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(values.email) &&
      values.password.length >= 4
    ) {
      setDisableBtn(false);
    } else {
      setDisableBtn(true);
    }
  }, [values]);

  return (
    <Box className={styles.bgBlue}>
      <Container>
        <Card sx={{ maxWidth: 600, margin: "auto" }}>
          <CardHeader className={styles.containerCardHeader} title="LOGIN" />
          <CardContent
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              src="/images/logo/logo-evp.png"
              alt=""
              width="136"
              height="100"
            />

            <FormControl
              sx={{ mt: 3, borderRadius: "10px", width: 400, maxWidth: "100%" }}
              size="small"
              variant="outlined"
            >
              <OutlinedInput
                id="outlined-adornment-password"
                type="text"
                value={values.email}
                onChange={handleChange("email")}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      edge="end"
                    >
                      <EmailIcon />
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>

            <FormControl
              sx={{ mt: 3, borderRadius: "10px", width: 400, maxWidth: "100%" }}
              size="small"
              variant="outlined"
            >
              <OutlinedInput
                id="outlined-adornment-password"
                type={showPassword ? "text" : "password"}
                value={values.password}
                onChange={handleChange("password")}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>

            <Typography className="textSubHeader fontMedium" sx={{ mt: 2 }}>
              Sign in to continue
            </Typography>
          </CardContent>
          <CardActions className={styles.containerButton} sx={{ pb: 10 }}>
            <Button
              size="small"
              disabled={disableBtn}
              fullWidth
              className={styles.btnGray}
              onClick={login}
            >
              SIGN IN
            </Button>
          </CardActions>
        </Card>
      </Container>

      <ModalChooseStation
        handleCloseModal={handleCloseModal}
        openModal={openModal}
        station={station}
      />
    </Box>
  );
}
