import React, { useState, useEffect, useContext } from "react";
import Header from "../header";
import styles from "./ManagementList.module.scss";
// import  {numberWithCommas}  from "@/utils/numberWithComma ";
//managementList
import {
  Box,
  Container,
  FormControl,
  Grid,
  Select,
  MenuItem,
  Typography,
  Button,
} from "@mui/material";
// import DataStation from "@/public/dataJson/station.json";
import Image from "next/image";

import Swal from "sweetalert2";
import { useRouter } from "next/router";
import useLoading from "@/hooks/useLoading";
import { errorAlert, warningAlert } from "@/utils/alert";
import apiFetch from "@/helpers/interceptors";
import AuthContext from "@/context/AuthProvider";

export default function ManagementList() {
  const { setIsLoading } = useLoading();
  const router = useRouter();
  const [station, setStation] = useState([]);
  const [chooseStation, setChooseStation] = useState(
    "เลือกแท่นชาร์จที่ต้องการ"
  );
  const [dataStation, setDataStation] = useState([]);
  const context = useContext(AuthContext);

  const [chargerdata, setChargerdata] = useState([]);
  const [selectedValue, setSelectedValue] = useState("");
  

  const [selectedValuedata, setSelectedValuedata] = useState([]);

  //function คลิ๊ก show data ที่เรา mapมา show
  const handleDropdown = (e) => {
    let val = e.target.value.chargePointName;
    console.log(val, "val");
    console.log(dataStation);
    type(val);
  };


  // const clear
  //function คลิ๊ก show data filter
  const type = (category) => {
    const filter = dataStation.filter((restaurant) => {
      return restaurant.ChargerDetails.chargePointName === category;
    });
    console.log(filter, "11111");
    setSelectedValuedata(filter)

  };

  const controlCharging = (id) => {
    Swal.fire({
      title: `ต้องการ${status ? "ปิด" : "เปิด"}แท่นชาร์จ?`,
      text: `${name}`,
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#303439",
      cancelButtonColor: "#CBCBCB",
      confirmButtonText: "ยืนยัน",
      cancelButtonText: "ยกเลิก",
    }).then((result) => {
      if (result.isConfirmed) {
        const dataChange = dataStation.map((item) => {
          if (item.id === id) {
            item.ChargerDetails.STATUS =
              item.ChargerDetails.STATUS !== "MAINTENANCE" ||
              item.ChargerDetails.STATUS !== "PENDING"
                ? "PENDING"
                : "AVAILABLE";
          }
          return item;
        });
        console.log("dataChange", dataChange);
        setDataStation(dataChange);
        Swal.fire({
          position: "center",
          icon: "success",
          title: `${status ? "ปิด" : "เปิด"}ใช้แท่นชาร์จสำเร็จ`,
          showConfirmButton: false,
          timer: 2000,
        });
      }
    });
  };

  //api dropdown Charger
  const getApidropdown = async () => {
    const idadmin = "6412a9fa43091da4da5d7808";
    setIsLoading(true);
    try {
      const res = await apiFetch.get(
        `/stations/mylist/namecharger?_id=${idadmin}`
      );
      if (res.data.res_code === "0000") {
        setChargerdata(res.data.res_data);
        setSelectedValue(res.data.res_data);
        // getApi()
        console.log(res.data.res_data, "Drops");
      } else {
        warningAlert(res.data.res_message);
      }
    } catch (error) {
      errorAlert(error);
    }
    setIsLoading(false);
  };

  //Apidatalist
  const getApi = async () => {
    setIsLoading(true);
    try {
      const body = {
        _id: "6412a9fa43091da4da5d7808",
        chargePointName: "",
      };
      const res = await apiFetch.post(
        `/stations/mylist/detailstation?sorting=ASC&perPage=2&page=1`,
        body
      );
      if (res.data.res_code === "0000") {
        setDataStation(res.data.res_data);
        setSelectedValuedata(res.data.res_data)
        console.log(res.data.res_data);
        const dataName = res.data.res_data.map((item) => {
          return {
            name: item.chargePointName,
          };
        });
        dataName.unshift({ name: "เลือกแท่นชาร์จที่ต้องการ" });
        setStation(dataName);
      } else {
        warningAlert(res.data.res_message);
      }
    } catch (error) {
      errorAlert(error);
    }
    setIsLoading(false);
  };

  const goPage = () => {
    router.push("/management/t?type=management-history");
  };

  useEffect(() => {
    getApi();
  }, []);

  useEffect(() => {
    getApidropdown();
  }, []);






  return (
    <Box sx={{ width: "100%" }}>
      <Header title={"Charger Management รายการแท่นชาร์จ"} />
      <Box className="bgSky" sx={{ minHeight: "90vh" }}>
        <Container>
          <Grid container sx={{ pt: 3, py: 3 }}>
            <Grid item xs={12} sm={6} lg={4}>
              <FormControl fullWidth>
                <Select
                  value={selectedValue}
                  displayEmpty
                  onChange={handleDropdown}
                  size="small"
                  sx={{
                    background: "#fff",
                    boxShadow: "none",
                  }}
                >
                  {chargerdata?.map((item, index) => (
                    <MenuItem key={index} value={item}>
                      {item.chargePointName}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container columnSpacing={{ xs: 1 }} rowSpacing={{ xs: 2 }}>
            {selectedValuedata.map((item, index) => (
              <Grid item xs={12} sm={6} lg={4} key={index}>
                <Box className={styles.containerCard}>
                  <Box className={styles.containerImage}>
                    {/* รุปตรง src ต้องให้หลังบ้านส่งค่ารูปมาด้วย */}
                    <Image
                      className={styles.img}
                      src="/images/station/60-kW-with-cements.png"
                      alt=""
                      width="0"
                      height="0"
                      sizes="100vw"
                    />
                  </Box>

                  <Typography
                    sx={{ my: 2, textAlign: "center" }}
                    className="textBlue fontBold textSubHeader"
                  >
                    {/*chargePointName*/}
                  </Typography>

                  <Container>
                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <Typography className="textBlue text14px">
                        สถานะการใช้งาน:
                      </Typography>
                      <Typography
                        className={
                          item.ChargerDetails.STATUS !== "MAINTENANCE" ||
                          item.ChargerDetails.STATUS !== "PENDING"
                            ? "textGreen text14px"
                            : "textRed text14px"
                        }
                      >
                        {item.ChargerDetails.STATUS !== "MAINTENANCE" ||
                        item.ChargerDetails.STATUS !== "PENDING"
                          ? // item.ChargerDetails.STATUSMachine === "ONLINE"
                            "ออนไลน์"
                          : "ออฟไลน์"}
                      </Typography>
                    </Box>

                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        mt: 1,
                      }}
                    >
                      <Typography className="textBlue text14px">
                        สถานะการทำงาน:
                      </Typography>
                      <Typography
                        className={
                          item.ChargerDetails.STATUS === "CHARGING"
                            ? "textGreen"
                            : item.ChargerDetails.STATUS === "PENDING"
                            ? "textOrange"
                            : item.ChargerDetails.STATUS === "AVAILABLE"
                            ? "textYellow"
                            : item.ChargerDetails.STATUS === "MAINTENANCE"
                            ? "textRed"
                            : "textBlue "
                        }
                        sx={{ fontSize: "14px" }}
                      >
                        {item.ChargerDetails.STATUS === "CHARGING"
                          ? "กำลังชาร์จไฟ"
                          : item.ChargerDetails.STATUS === "PENDING"
                          ? "ไม่สามารถใช้งานได้"
                          : item.ChargerDetails.STATUS === "MAINTENANCE"
                          ? "กำลังซ่อมบำรุง"
                          : item.ChargerDetails.STATUS === "PREPARING"
                          ? "เตรียมพร้อมชาร์จ"
                          : "ไม่มีการชาร์จ"}
                      </Typography>
                    </Box>

                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        mt: 1,
                      }}
                    >
                      <Typography className="textBlue text14px">
                        จำนวนการชาร์จทั้งหมด:
                      </Typography>
                      <Typography className="text14px">
                        {item.ChargerDetails.TotalTransactionCount}
                      </Typography>
                    </Box>

                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        mt: 1,
                      }}
                    >
                      <Typography className="textBlue text14px">
                        เวลาการชาร์จรวมทั้งหมด:
                      </Typography>
                      <Typography className="text14px">
                        {item.ChargerDetails.finalDuration.Hour}
                        &nbsp;ชม.&nbsp;
                        {item.ChargerDetails.finalDuration.Minute}&nbsp;นาที
                      </Typography>
                    </Box>

                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        mt: 1,
                      }}
                    >
                      <Typography className="textBlue text14px">
                        อัตราการใช้พลังงานทั้งหมด:
                      </Typography>
                      <Typography className="text14px">
                        {item.ChargerDetails.TotalMeter}
                        &nbsp;kWh
                      </Typography>
                    </Box>

                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        mt: 1,
                      }}
                    >
                      <Typography className="textBlue text14px">
                        จำนวนเงินที่ได้รับทั้งหมด:
                      </Typography>
                      <Typography className="text14px">
                        {item.ChargerDetails.TotalIncome}&nbsp;บาท
                      </Typography>
                    </Box>

                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                        mt: 1,
                      }}
                    >
                      <Typography className="textBlue text14px">
                        ควบคุมแท่นชาร์จ:
                      </Typography>

                      <Box sx={{ display: "flex" }}>
                        {(item.ChargerDetails.STATUS !== "MAINTENANCE" ||
                          item.ChargerDetails.STATUS !== "PENDING") && (
                          <Box
                            className={styles.toggleSwitch1}
                            onClick={() =>
                              controlCharging(
                                item.id,
                                item.usageStatus,
                                item.name
                              )
                            }
                          >
                            ปิด
                          </Box>
                        )}
                        {(item.ChargerDetails.STATUS !== "MAINTENANCE" ||
                          item.ChargerDetails.STATUS !== "PENDING") && (
                          <Button className="btnGreen">เปิด</Button>
                        )}

                        {(item.ChargerDetails.STATUS === "MAINTENANCE" ||
                          item.ChargerDetails.STATUS === "PENDING") && (
                          <Button className="btnRed">ปิด</Button>
                        )}

                        {(item.ChargerDetails.STATUS === "MAINTENANCE" ||
                          item.ChargerDetails.STATUS === "PENDING") && (
                          <Box
                            className={styles.toggleSwitch2}
                            onClick={() =>
                              controlCharging(
                                item.ChargerDetails.id,
                                item.ChargerDetails.usageStatus,
                                item.ChargerDetails.name
                              )
                            }
                          >
                            เปิด
                          </Box>
                        )}
                      </Box>
                    </Box>

                    <Button
                      fullWidth
                      sx={{ my: 2, borderRadius: "5px" }}
                      className="btnBlack"
                      onClick={() => goPage()}
                    >
                      ประวัติการชาร์จ
                    </Button>
                  </Container>
                </Box>
              </Grid>
            ))}
          </Grid>
        </Container>
      </Box>
    </Box>
  );
}
