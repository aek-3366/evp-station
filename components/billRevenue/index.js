import React, { useState } from "react";
import styles from "./BillRevenue.module.scss";
import Header from "../header";
import {
  Box,
  Container,
  Grid,
  TextField,
  Typography,
  Button,
} from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

import { styled } from "@mui/material/styles";
import BillRevenueTable from "./BillRevenueTable";
import ContactPhoneIcon from "@mui/icons-material/ContactPhone";

const TextFieldStyle = styled(TextField)(({ theme }) => ({
  "& .MuiOutlinedInput-notchedOutline": {
    border: "none",
  },
}));

export default function BillRevenue() {
  const [time, setTime] = useState({
    start: "",
    end: "",
  });
  return (
    <Box sx={{ width: "100%" }}>
      <Header title={"Bill รายได้ของร้าน"} />
      <Box className="bgSky" sx={{ minHeight: "90vh" }}>
        <Container sx={{ pt: 4 }}>
          <Grid container rowSpacing={{ xs: 2 }} columnSpacing={{ xs: 1 }}>
            <Grid item xs={12} sm={6} md={4}>
              <Box className={styles.cardBlue} sx={{ py: 2 }}>
                <Typography>รายได้รวมแท่นชาร์จทั้งหมด</Typography>
                <Typography sx={{ mt: 1 }}>00,000,000 บาท</Typography>
              </Box>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Box className={styles.cardGray} sx={{ py: 2 }}>
                <Typography>รายได้รวม</Typography>
                <Typography sx={{ mt: 1 }}>00,000,000 บาท</Typography>
              </Box>
            </Grid>
            <Grid item xs={12} sm={6} md={4} sx={{ py: 2 }}>
              <Box className={styles.cardYellow} sx={{ py: 2 }}>
                <Typography>รายได้สุทธิที่ได้รับ</Typography>
                <Typography sx={{ mt: 1 }}>00,000,000 บาท</Typography>
              </Box>
            </Grid>
          </Grid>

          <Grid container rowSpacing={{ xs: 2 }} columnSpacing={{ xs: 1 }}>
            <Grid item xs={12} sm={6}>
              <Box className={styles.boxWithdrawal} sx={{ p: 1 }}>
                <Box className={styles.containerIcon}>
                  <ContactPhoneIcon />
                </Box>
                <Typography sx={{ ml: 1 }}>ติดต่อทำรายการถอนเงิน</Typography>
              </Box>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Grid
                container
                direction="row"
                alignItems="center"
                rowSpacing={{ xs: 2 }}
                columnSpacing={{ xs: 1 }}
              >
                <Grid item xs={12} sm={5}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      inputFormat="MM/DD/YYYY"
                      value={time.start}
                      onChange={(e) => setTime({ ...time, start: e })}
                      renderInput={(params) => (
                        <TextFieldStyle
                          {...params}
                          inputProps={{
                            ...params.inputProps,
                            placeholder: "Start Date",
                          }}
                          fullWidth
                          size="small"
                          sx={{
                            background: "#fff",
                            borderRadius: "4px",
                            border: "1px solid #555555",
                          }}
                        />
                      )}
                    />
                  </LocalizationProvider>
                </Grid>

                <Grid item xs={12} sm={5}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      inputFormat="MM/DD/YYYY"
                      value={time.end}
                      onChange={(e) => setTime({ ...time, end: e })}
                      renderInput={(params) => (
                        <TextFieldStyle
                          {...params}
                          inputProps={{
                            ...params.inputProps,
                            placeholder: "End Date",
                          }}
                          fullWidth
                          size="small"
                          sx={{
                            background: "#fff",
                            borderRadius: "4px",
                            border: "1px solid #555555",
                          }}
                        />
                      )}
                    />
                  </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={2}>
                  <Button className="btnBlue" fullWidth>
                    ค้นหา
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Box sx={{ mt: 3 }}>
            <BillRevenueTable />
          </Box>
        </Container>
      </Box>
    </Box>
  );
}
