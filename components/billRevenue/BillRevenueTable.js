import React, { useState } from "react";
import styles from "./BillRevenue.module.scss"
import { styled } from "@mui/material/styles";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import {
  Box,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Pagination,
  Modal,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import Image from "next/image";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    //   color: theme.palette.common.white,
    color: "000000",
    backgroundColor: "#FDE1E9",
    borderBottom: "none",
    whiteSpace: "nowrap",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    backgroundColor: "#FCFCFC",
    border: 0,
    color: "#000000",
    borderBottom: "none",
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    borderBottom: "none",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    borderBottom: "none",
  },
}));

function createData(
  order,
  date,
  time,
  revenueCharging,
  revenueTotal,
  noticeWithdrawal,
  netIncome,
  status,
  slip
) {
  return {
    order,
    date,
    time,
    revenueCharging,
    revenueTotal,
    noticeWithdrawal,
    netIncome,
    status,
    slip,
  };
}

const rows = [
  createData(
    "0001",
    "14 ธ.ค. 66",
    "18:27",
    "00,000,000",
    "00,000,000",
    "150,000",
    "140,000",
    "สำเร็จ",
    "/images/orange-cat.jpeg"
  ),
];

const tableHead = [
  "รายการ",
  "วันที่",
  "เวลา",
  "รายได้รวมแท่ยชาร์จทั้งหมด",
  "รายได้รวม",
  "รายการแจ้งถอนเงิน",
  "รายได้สุทธิที่ได้รับ",
  "สถานะ",
  "ใบเสร็จ",
];

export default function BillRevenueTable() {
  const [showImage, setShowImage] = useState("");
  const [open, setOpen] = useState(false);
  const handleOpen = (id) => {
    const filterImg = rows.find((item) => item.id === id);
    setShowImage(filterImg.slip);
    setOpen(true);
  };
  const handleClose = () => {
    setShowImage("");
    setOpen(false);
  };
  return (
    <>
      <Box className="tableHeader">ประวัติการขอถอนเงิน</Box>
      <TableContainer component={Paper}>
        <Table
          sx={{ background: "#000000", minWidth: 700 }}
          aria-label="customized table"
        >
          <TableHead>
            <TableRow>
              {tableHead.map((item, index) => (
                <StyledTableCell key={index} align="center">
                  {item}
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row, index) => (
              <StyledTableRow key={index}>
                <StyledTableCell component="th" scope="row">
                  {row.order}
                </StyledTableCell>
                <StyledTableCell align="center">{row.date}</StyledTableCell>
                <StyledTableCell align="center">{row.time}</StyledTableCell>
                <StyledTableCell align="center">
                  {row.revenueCharging}
                </StyledTableCell>
                <StyledTableCell align="center" sx={{ minWidth: "120px" }}>
                  {row.revenueTotal}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {row.noticeWithdrawal}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {row.netIncome}
                </StyledTableCell>
                <StyledTableCell align="center">{row.status}</StyledTableCell>
                <StyledTableCell align="center">
                  <Image
                    src={row.slip}
                    alt=""
                    width="75"
                    height="75"
                    style={{ cursor: "pointer" }}
                    onClick={() => handleOpen(row.id)}
                  />
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <Box sx={{ display: "flex", justifyContent: "flex-end", mt: 2 }}>
        <Pagination
          count={rows.length}
          showFirstButton
          showLastButton
          variant="outlined"
          shape="rounded"
        />
      </Box>

      <Modal open={open} onClose={handleClose} className={styles.modalBgColor}>
        <Box className={styles.modalBoxImg}>
          <Box sx={{ display: "flex", justifyContent: "flex-end", mb: 1 }}>
            <CloseIcon
              sx={{ cursor: "pointer" }}
              onClick={() => handleClose()}
            />
          </Box>
          <Box sx={{ textAlign: "center" }}>
            <Image
              src={showImage}
              alt=""
              width="0"
              height="0"
              sizes="100"
              className={styles.img}
            />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
