import { Box, Typography } from "@mui/material";
import React from "react";
import styles from "./Header.module.scss";

export default function Header({title}) {
  return (
    <Box className={styles.containerHeader}>
      <Typography sx={{ color: "#fff" }} className="textHeader">
        {title}
      </Typography>
    </Box>
  );
}
