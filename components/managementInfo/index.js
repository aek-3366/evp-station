import React, { useEffect, useState } from "react";
import Header from "../header";
import styles from "./ManagementInfo.module.scss";
import { styled } from "@mui/material/styles";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import { getCookie } from "cookies-next";

import {
  Box,
  Container,
  Grid,
  Paper,
  InputBase,
  Button,
  TextField,
  FormControl,
  Select,
  MenuItem,
} from "@mui/material";

import {
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Pagination,
} from "@mui/material";

// import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
// import { DatePicker } from "@mui/x-date-pickers/DatePicker";
// import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
// import ManageInfoTable from "./ManageInfoTable";
import ModalInfo from "@/components/modal/ModalInfo";
import useLoading from "@/hooks/useLoading";
import apiFetch from "@/helpers/interceptors";

const TextFieldStyle = styled(TextField)(({ theme }) => ({
  "& .MuiOutlinedInput-notchedOutline": {
    border: "none",
  },
}));

const type = [
  {
    id: 0,
    value: "เลือกแท่นชาร์จที่ต้องการค้นหา",
  },
  {
    id: 1,
    value: "1",
  },
  {
    id: 2,
    value: "2",
  },
  {
    id: 3,
    value: "3",
  },
];

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    //   color: theme.palette.common.white,
    color: "000000",
    backgroundColor: "#FDE1E9",
    borderBottom: "none",
    whiteSpace: "nowrap",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    backgroundColor: "#FCFCFC",
    border: 0,
    color: "#000000",
    borderBottom: "none",
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    borderBottom: "none",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    borderBottom: "none",
  },
}));

function createData(
  idCharge,
  product,
  type,
  body,
  length,
  maxPower,
  electric,
  location
) {
  return {
    idCharge,
    product,
    type,
    body,
    length,
    maxPower,
    electric,
    location,
  };
}

const rows = [
  createData(
    "EVP0001",
    "MULTO-DC Charger",
    "TYPE2 / CCS2",
    "750X630X1800 มิลลิเมตร",
    "5 เมตร",
    "DC120 kW+ AC22kw",
    "187A+ 251 3Phase"
  ),
];

const tableHead = [
  "รหัสแท่นชาร์จ",
  "ชื่อแท่นชาร์จ",
  "ชื่อผลิตภัณฑ์",
  "ชนิดหัวชาร์จ",
  "ขนาดตัวเครื่อง",
  "ความยาวสายชาร์จ",
  "กำลังไฟสูงสุด",
  "กระแสไฟ",
  "สถานที่ตั้ง",
];

export default function ManagementInfo() {
  const [time, setTime] = useState({
    start: "",
    end: "",
  });

  const { setIsLoading } = useLoading();
  const [station, setStation] = useState("เลือกแท่นชาร์จที่ต้องการค้นหา");
  const [openModal, setOpenModal] = useState(false);
  const [data, setData] = useState({});

  const [chargerLis, setChargerLis] = useState([]); //Listdata
  const [dataInmodal, setDataInmodal] = useState([]); //Listdata
  const [perPage, setPerPage] = useState(2); // ขอมูลออกมาทีละ 2 ชุด
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [chagerSelect, setChagerSelect] = useState([]);
  const id = getCookie("_id");

  const [searchingdata, setSearchdata] = useState("");
  const [chargePointName, setChargePointName] = useState("");

  const seacrh = () => {
    console.log(searchingdata,'searchingdata')
    console.log(chargePointName,'chargePointName')


    const body = {
      searchingdata: searchingdata,
      chargePointName: chargePointName,
    };
    getChargerListdata(body);
  };

  const handeOpenModal = (id) => {
    const dataFilter = chargerLis.find((item) => item.idCharge === id);
    setDataInmodal(dataFilter);
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleChangePagination = (event, value) => {
    // console.log("หน้าPage", value);
    setPage(value);
  };

  useEffect(() => {
    getChargerListdata();
  }, [page]);

  useEffect(() => {
    getApicharging();
  }, []);

  //เลือกแท่นชาร์จที่ต้องการค้นหา
  const getApicharging = async () => {
    setIsLoading(true);
    try {
      const res = await apiFetch.get(
        `/stations/mylist/namecharger/noobjectname/?_id=${id}`
      );
      if (res.data.res_code === "0000") {
        setChagerSelect(res.data.res_data);
        console.log('"Chagerแท่นชาร์จ"', res.data.res_data);
      } else {
        warningAlert(res.data.res_message);
      }
    } catch (error) {
      errorAlert(error);
    }
    setIsLoading(false);
  };

  //getChargerListdata
  const getChargerListdata = async (data) => {
    setIsLoading(true);
    try {
      const body = {
        _id: id,
        searchingdata: data?.searchingdata || '',
        chargePointName: data?.chargePointName || '',
      };
      const res = await apiFetch.post(
        `/stations/mylist/detailsample/charger?sorting=ASC&perPage=${perPage}&page=${page}`,
        body
      );

      if (res.data.res_code === "0000") {
        setChargerLis(res.data.res_data.DetailSampleCharger);
        setCount(Math.ceil(res.data.res_data.count / perPage));
        console.log("chargerList", res.data.res_data.DetailSampleCharger);
        // console.log(res.data.res_data.count, "count");
      } else {
        warningAlert(res.data.res_message);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Header title={"Charger Management ข้อมูลแท่นชาร์จ"} />
      <Box className="bgSky" sx={{ minHeight: "90vh" }}>
        <Container sx={{ pt: 4 }}>
          <Grid container rowSpacing={{ xs: 2 }} columnSpacing={{ xs: 1 }}>
            <Grid item xs={12} sm={5} md={6}>
              <Box className={styles.containerBox1}>
                <Paper
                  component="form"
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    border: "1px solid #555555",
                    borderRight: "none",
                  }}
                >
                  <InputBase  
                  value={searchingdata}
                  onChange={(e) => setSearchdata(e.target.value)}
                  placeholder="ค้นหาข้อมูล" sx={{ pl: 1, flex: 1 }} />

                  <Button className="btnBlue"
                  onClick={() => seacrh()}
                  >
                  ค้นหา</Button>
                </Paper>
              </Box>
            </Grid>

            <Grid item xs={12} sm={7} md={6}>
              <Grid
                container
                direction="row"
                alignItems="center"
                rowSpacing={{ xs: 2 }}
                columnSpacing={{ xs: 1 }}
                justifyContent="right"
              >
                <Grid item xs={12} sm={5} md={6}>
                  <Box className={styles.containerBox1}>
                    <FormControl fullWidth>
                      <Select
                        value={chargePointName}
                        displayEmpty
                        onChange={(e) => setChargePointName(e.target.value)}
                        size="small"
                        sx={{
                          background: "#fff",
                          boxShadow: "none",
                        }}
                      >
                        {chagerSelect.map((item, index) => (
                          <MenuItem key={index} value={item}>
                            {item}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Box>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          {/* */}
          <Box sx={{ mt: 3 }}>
            <Box className="tableHeader">ข้อมูลแท่นชาร์จ</Box>
            <TableContainer component={Paper}>
              <Table
                sx={{ background: "#000000", minWidth: 700 }}
                aria-label="customized table"
              >
                <TableHead>
                  <TableRow>
                    {tableHead.map((item, index) => (
                      <StyledTableCell key={index} align="center">
                        {item}
                      </StyledTableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {chargerLis?.map((row, index) => (
                    <StyledTableRow key={index}>
                      <StyledTableCell component="th" scope="row">
                        {row.NumberCharger}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.chargePointName}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.DetailSampleCharger.NameCharger}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.DetailSampleCharger.HeaderChargerType}
                      </StyledTableCell>
                      <StyledTableCell
                        align="center"
                        sx={{ minWidth: "120px" }}
                      >
                        {row.DetailSampleCharger.SizeCharger}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.DetailSampleCharger.ChargerCableLength}
                      </StyledTableCell>

                      <StyledTableCell align="center">
                        {row.DetailSampleCharger.ElectricPower}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.DetailSampleCharger.ElectricCurrent}
                      </StyledTableCell>

                      <StyledTableCell
                        align="center"
                        className="textBlue"
                        sx={{ cursor: "pointer" }}
                        onClick={() => handeOpenModal(row._id)}
                      >
                        ดูข้อมูล
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>

            <Box sx={{ display: "flex", justifyContent: "flex-end", mt: 2 }}>
              <Pagination
                count={count}
                showFirstButton
                showLastButton
                variant="outlined"
                shape="rounded"
                page={page}
                onChange={handleChangePagination}
              />
            </Box>

            <ModalInfo
              dataInmodal={dataInmodal}
              openModal={openModal}
              handleCloseModal={handleCloseModal}
            />
          </Box>
        </Container>
      </Box>
    </Box>
  );
}
