import React,{useState} from "react";
import { styled } from "@mui/material/styles";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import {
  Box,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Pagination,
} from "@mui/material";
import ModalInfo from "@/components/modal/ModalInfo";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    //   color: theme.palette.common.white,
    color: "000000",
    backgroundColor: "#FDE1E9",
    borderBottom: "none",
    whiteSpace: "nowrap",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    backgroundColor: "#FCFCFC",
    border: 0,
    color: "#000000",
    borderBottom: "none",
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    borderBottom: "none",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    borderBottom: "none",
  },
}));

function createData(
  idCharge,
  product,
  type,
  body,
  length,
  maxPower,
  electric,
  location
) {
  return {
    idCharge,
    product,
    type,
    body,
    length,
    maxPower,
    electric,
    location,
  };
}

const rows = [
  createData(
    "EVP0001",
    "MULTO-DC Charger",
    "TYPE2 / CCS2",
    "750X630X1800 มิลลิเมตร",
    "5 เมตร",
    "DC120 kW+ AC22kw",
    "187A+ 251 3Phase"
  ),
];

const tableHead = [
  "รหัสแท่นชาร์จ",
  "ชื่อผลิตภัณฑ์",
  "ชนิดหัวชาร์จ",
  "ขนาดตัวเครื่อง",
  "ความยาวสายชาร์จ",
  "กำลังไฟสูงสุด",
  "กระแสไฟ",
  "สถานที่ตั้ง",
];

export default function ManageInfoTable() {
  const [openModal, setOpenModal] = useState(false);
  const [data, setData] = useState({});

  const handeOpenModal = (id) => {
    // const dataFilter = rows.find((item) => item.idCharge === id);
    // setData(dataFilter);
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };
  return (
    <>
      <Box className="tableHeader">ข้อมูลแท่นชาร์จ</Box>
      <TableContainer component={Paper}>
        <Table
          sx={{ background: "#000000", minWidth: 700 }}
          aria-label="customized table"
        >
          <TableHead>
            <TableRow>
              {tableHead.map((item, index) => (
                <StyledTableCell key={index} align="center">
                  {item}
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row, index) => (
              <StyledTableRow key={index}>
                <StyledTableCell component="th" scope="row">
                  {row.idCharge}
                </StyledTableCell>
                <StyledTableCell align="center">{row.product}</StyledTableCell>
                <StyledTableCell align="center">{row.type}</StyledTableCell>
                <StyledTableCell align="center">{row.body}</StyledTableCell>
                <StyledTableCell align="center" sx={{ minWidth: "120px" }}>
                  {row.length}
                </StyledTableCell>
                <StyledTableCell align="center">{row.maxPower}</StyledTableCell>
                <StyledTableCell align="center">{row.electric}</StyledTableCell>
                <StyledTableCell
                  align="center"
                  className="textBlue"
                  sx={{ cursor: "pointer" }}
                  onClick={() => handeOpenModal(row.idCharge)}
                >
                  ดูข้อมูล
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <Box sx={{ display: "flex", justifyContent: "flex-end", mt: 2 }}>
        <Pagination
          count={rows.length}
          showFirstButton
          showLastButton
          variant="outlined"
          shape="rounded"
        />
      </Box>

      <ModalInfo
        openModal={openModal}
        handleCloseModal={handleCloseModal}
        data={data}
      />
    </>
  );
}
