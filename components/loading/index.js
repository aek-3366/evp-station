import React from "react";
import styles from "./Loading.module.scss";

export default function Loading() {
  return (
    <>
      <div className={styles.containerX}>
        <div className={styles.holder}>
          <div className={styles.car}></div>
          <div className={styles.wheels}></div>
          <div className={styles.windHolder}>
            <div className={styles.wind}></div>
          </div>
          <div className={`${styles.tree} ${styles.tree1}`}></div>
          <div className={`${styles.tree} ${styles.tree2}`}></div>
          <div className={`${styles.tree} ${styles.tree3}`}></div>
          <div className={`${styles.hill} ${styles.hill1}`}></div>
          <div className={`${styles.hill} ${styles.hill2}`}></div>
          <div className={`${styles.cloud} ${styles.cloud1}`}></div>
          <div className={`${styles.cloud} ${styles.cloud1_2}`}></div>
          <div className={`${styles.cloud} ${styles.cloud2}`}></div>
          <div className={`${styles.cloud} ${styles.ccloud2_2}`}></div>
        </div>

        <div className={styles.textLoading}>กำลังโหลดข้อมูล</div>
      </div>

      {/* <div className={styles.containerX}>
      <div className={`${styles.circle}`}></div>
      <span id={styles.loading" style={{ color: "#fff" }}>
        กำลังโหลดข้อมูล
      </span>
    </div> */}
    </>
  );
}
