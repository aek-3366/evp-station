import React, { useState } from "react";
import styles from "./Sidebar.module.scss";
import { Box, Button, Container, Typography } from "@mui/material";
import Image from "next/image";
import { useRouter } from "next/router";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { deleteCookie } from "cookies-next";

export default function Sidebar() {
  const router = useRouter();
  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const logout = () => {
    deleteCookie('accInfo')
    deleteCookie('_id')
    deleteCookie('token')
    router.replace("/");
  };

  return (
    <Box className={styles.containerSidebar}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          mt: 3,
        }}
      >
        <Image
          src="/images/logo/logo-evp.png"
          alt=""
          width="150"
          height="150"
        />
      </Box>

      <Box className={styles.line} sx={{ my: 3 }}></Box>

      <Container>
        <Accordion
          expanded={expanded === "panel1"}
          onChange={handleChange("panel1")}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon sx={{ color: "#000000" }} />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography
              className={`fontBold ${
                router.pathname.includes("management") && "textBlue"
              }`}
            >
              Charger Management
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Box
              sx={{ display: "flex", cursor: "pointer" }}
              onClick={() => router.push("/management/t?type=management-list")}
              className={`${styles.textHover} ${
                router.query.type === "management-list" && "textBlue"
              }`}
            >
              <Typography sx={{ mr: 2 }}>-</Typography>
              <Typography>รายการแท่นชาร์จ</Typography>
            </Box>
            <Box
              sx={{ display: "flex", mt: 1, cursor: "pointer" }}
              onClick={() =>
                router.push("/management/t?type=management-history")
              }
              className={`${styles.textHover} ${
                router.query.type === "management-history" && "textBlue"
              }`}
            >
              <Typography sx={{ mr: 2 }}>-</Typography>
              <Typography>ประวัติการใช้งาน</Typography>
            </Box>
            <Box
              sx={{ display: "flex", mt: 1, cursor: "pointer" }}
              onClick={() =>
                router.push("/management/t?type=management-infomation")
              }
              className={`${styles.textHover} ${
                router.query.type === "management-infomation" && "textBlue"
              }`}
            >
              <Typography sx={{ mr: 2 }}>-</Typography>
              <Typography>ข้อมูลแท่นชาร์จ</Typography>
            </Box>
          </AccordionDetails>
        </Accordion>

        <Accordion
          expanded={expanded === "panel2"}
          onChange={handleChange("panel2")}
          sx={{ mt: 2 }}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon sx={{ color: "#000000" }} />}
            aria-controls="panel2bh-content"
            id="panel2bh-header"
          >
            <Typography
              className={`fontBold ${
                router.pathname.includes("bill") && "textBlue"
              }`}
            >
              Bill
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Box
              sx={{ display: "flex", cursor: "pointer" }}
              onClick={() => router.push("/bill/t?type=bill-history")}
              className={`${styles.textHover} ${
                router.query.type === "bill-history" && "textBlue"
              }`}
            >
              <Typography sx={{ mr: 2 }}>-</Typography>
              <Typography>ประวัติการจ่ายบิล</Typography>
            </Box>
            <Box
              sx={{ display: "flex", mt: 1, cursor: "pointer" }}
              onClick={() => router.push("/bill/t?type=bill-revenue")}
              className={`${styles.textHover} ${
                router.query.type === "bill-revenue" && "textBlue"
              }`}
            >
              <Typography sx={{ mr: 2 }}>-</Typography>
              <Typography>รายได้ของร้าน</Typography>
            </Box>
          </AccordionDetails>
        </Accordion>

        <Button
          className="btnGray"
          size="small"
          fullWidth
          sx={{ mt: 3, borderRadius: "10px" }}
          onClick={() => logout()}
        >
          SIGN OUT
        </Button>
      </Container>
    </Box>
  );
}
