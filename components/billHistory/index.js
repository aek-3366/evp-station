import React, { useState } from "react";
import Header from "../header";
import styles from "./BillHistory.module.scss";
import {
  Box,
  Container,
  Grid,
  Paper,
  InputBase,
  Button,
  TextField,
  FormControl,
  Select,
  MenuItem,
} from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import BillHistoryTable from "./BillHistoryTable";

import { styled } from "@mui/material/styles";

const TextFieldStyle = styled(TextField)(({ theme }) => ({
  "& .MuiOutlinedInput-notchedOutline": {
    border: "none",
  },
}));

const type = [
  {
    id: 0,
    value: "เลือกตู้ชาร์จ",
  },
  {
    id: 1,
    value: "1",
  },
  {
    id: 2,
    value: "2",
  },
  {
    id: 3,
    value: "3",
  },
];

export default function BillHistory() {
  const [time, setTime] = useState({
    start: "",
    end: "",
  });

  const [station, setStation] = useState("เลือกตู้ชาร์จ");
  return (
    <Box sx={{ width: "100%" }}>
      <Header title={"Bill ประวัติการจ่ายบิล"} />
      <Box className="bgSky" sx={{ minHeight: "90vh" }}>
        <Container sx={{ pt: 4 }}>
          <Grid container rowSpacing={{ xs: 2 }} columnSpacing={{ xs: 1 }}>
            <Grid item xs={12} sm={5} md={6}>
              <Box className={styles.containerBox1}>
                <Paper
                  component="form"
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    border: "1px solid #555555",
                    borderRight: "none",
                  }}
                >
                  <InputBase sx={{ pl: 1, flex: 1 }} />
                  <Button className="btnBlue">ค้นหา</Button>
                </Paper>
              </Box>
            </Grid>

            <Grid item xs={12} sm={7} md={6}>
              <Grid
                container
                direction="row"
                alignItems="center"
                rowSpacing={{ xs: 2 }}
                columnSpacing={{ xs: 1 }}
              >
                <Grid item xs={12} sm={5}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      inputFormat="MM/DD/YYYY"
                      value={time.start}
                      onChange={(e) => setTime({ ...time, start: e })}
                      renderInput={(params) => (
                        <TextFieldStyle
                          {...params}
                          inputProps={{
                            ...params.inputProps,
                            placeholder: "Start Date",
                          }}
                          fullWidth
                          size="small"
                          sx={{
                            background: "#fff",
                            borderRadius: "4px",
                            border: "1px solid #555555",
                          }}
                        />
                      )}
                    />
                  </LocalizationProvider>
                </Grid>

                <Grid item xs={12} sm={5}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      inputFormat="MM/DD/YYYY"
                      value={time.end}
                      onChange={(e) => setTime({ ...time, end: e })}
                      renderInput={(params) => (
                        <TextFieldStyle
                          {...params}
                          inputProps={{
                            ...params.inputProps,
                            placeholder: "End Date",
                          }}
                          fullWidth
                          size="small"
                          sx={{
                            background: "#fff",
                            borderRadius: "4px",
                            border: "1px solid #555555",
                          }}
                        />
                      )}
                    />
                  </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={2}>
                  <Button className="btnBlue" fullWidth>
                    ค้นหา
                  </Button>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12} sm={5} md={6}>
              <Box className={styles.containerBox1}>
                <FormControl fullWidth>
                  <Select
                    value={station}
                    displayEmpty
                    onChange={(e) => setStation(e.target.value)}
                    size="small"
                    sx={{
                      background: "#fff",
                      boxShadow: "none",
                    }}
                  >
                    {type.map((item) => (
                      <MenuItem key={item.id} value={item.value}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
            </Grid>
          </Grid>

          <Box sx={{mt: 3}}>
            <BillHistoryTable />
          </Box>
        </Container>
      </Box>
    </Box>
  );
}
