import React,{useState} from "react";
import styles from "./BillHistory.module.scss"
import { styled } from "@mui/material/styles";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import {
  Box,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Pagination,
  Modal
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import Image from "next/image";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    //   color: theme.palette.common.white,
    color: "000000",
    backgroundColor: "#FDE1E9",
    borderBottom: "none",
    whiteSpace: "nowrap",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    backgroundColor: "#FCFCFC",
    border: 0,
    color: "#000000",
    borderBottom: "none",
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    borderBottom: "none",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    borderBottom: "none",
  },
}));

function createData(
  idCharge,
  start,
  end,
  time,
  powerConsumption,
  methodOfCharge,
  totalAmount,
  methodOfPayment,
  slip
) {
  return {
    idCharge,
    start,
    end,
    time,
    powerConsumption,
    methodOfCharge,
    totalAmount,
    methodOfPayment,
    slip,
  };
}

const rows = [
  createData(
    "EVP0001",
    "15:00",
    "18:27",
    "2ชม. 27น.",
    "100kWh",
    "นาที",
    "1000 บาท",
    "MasterCard",
    "/images/orange-cat.jpeg"
  ),
  createData(
    "EVP0002",
    "16:00",
    "17:27",
    "2ชม. 27น.",
    "100kWh",
    "ชม.",
    "1000 บาท",
    "MasterCard",
    "/images/orange-cat.jpeg"
  ),
  createData(
    "EVP0003",
    "19:00",
    "21:27",
    "2ชม. 27น.",
    "100kWh",
    "kWh",
    "1000 บาท",
    "MasterCard",
    "/images/orange-cat.jpeg"
  ),
];

const tableHead = [
  "รหัสแท่นชาร์จ",
  "เริ่มชาร์จ",
  "สิ้นสุดการชาร์จ",
  "เวลาชาร์จรวม",
  "อัตราการใช้พลังงาน",
  "วิธีคิดเงิน",
  "จำนวนเงินรวม",
  "ช่องทางการชำระ",
  "ใบเสร็จ",
];

export default function BillHistoryTable() {
  const [showImage, setShowImage] = useState("");
  const [open, setOpen] = useState(false);
  const handleOpen = (id) => {
    const filterImg = rows.find((item) => item.id === id);
    setShowImage(filterImg.slip);
    setOpen(true);
  };
  const handleClose = () => {
    setShowImage("");
    setOpen(false);
  };
  return (
    <>
      <Box className="tableHeader">ประวัติการจ่ายบิล</Box>
      <TableContainer component={Paper}>
        <Table
          sx={{ background: "#000000", minWidth: 700 }}
          aria-label="customized table"
        >
          <TableHead>
            <TableRow>
              {tableHead.map((item, index) => (
                <StyledTableCell key={index} align="center">
                  {item}
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row, index) => (
              <StyledTableRow key={index}>
                <StyledTableCell component="th" scope="row">
                  {row.idCharge}
                </StyledTableCell>
                <StyledTableCell align="center">{row.start}</StyledTableCell>
                <StyledTableCell align="center">{row.end}</StyledTableCell>
                <StyledTableCell align="center">{row.time}</StyledTableCell>
                <StyledTableCell align="center" sx={{ minWidth: "120px" }}>
                  {row.powerConsumption}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {row.methodOfCharge}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {row.totalAmount}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {row.methodOfPayment}
                </StyledTableCell>
                <StyledTableCell align="center">
                  <Image
                    src={row.slip}
                    alt=""
                    width="75"
                    height="75"
                    style={{ cursor: "pointer" }}
                    onClick={() => handleOpen(row.id)}
                  />
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <Box sx={{ display: "flex", justifyContent: "flex-end", mt: 2 }}>
        <Pagination
          count={rows.length}
          showFirstButton
          showLastButton
          variant="outlined"
          shape="rounded"
        />
      </Box>

      <Modal open={open} onClose={handleClose} className={styles.modalBgColor}>
        <Box className={styles.modalBoxImg}>
          <Box sx={{ display: "flex", justifyContent: "flex-end", mb: 1 }}>
            <CloseIcon
              sx={{ cursor: "pointer" }}
              onClick={() => handleClose()}
            />
          </Box>
          <Box sx={{ textAlign: "center" }}>
            <Image
              src={showImage}
              alt=""
              width="0"
              height="0"
              sizes="100"
              className={styles.img}
            />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
