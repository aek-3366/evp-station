// import React, { useEffect, useState } from "react";
// import { styled } from "@mui/material/styles";
// import TableCell, { tableCellClasses } from "@mui/material/TableCell";
// import useLoading from "@/hooks/useLoading";
// import apiFetch from "@/helpers/interceptors";

// import {
//   Box,
//   Table,
//   TableBody, 
//   TableContainer,
//   TableHead,
//   TableRow,
//   Paper,
//   Pagination,
// } from "@mui/material";

// const StyledTableCell = styled(TableCell)(({ theme }) => ({
//   [`&.${tableCellClasses.head}`]: {
//     // backgroundColor: theme.palette.common.black,
//     //   color: theme.palette.common.white,
//     color: "000000",
//     backgroundColor: "#FDE1E9",
//     borderBottom: "none",
//     whiteSpace: "nowrap",
//   },
//   [`&.${tableCellClasses.body}`]: {
//     fontSize: 14,
//     backgroundColor: "#FCFCFC",
//     border: 0,
//     color: "#000000",
//     borderBottom: "none",
//   },
// }));

// const StyledTableRow = styled(TableRow)(({ theme }) => ({
//   "&:nth-of-type(odd)": {
//     borderBottom: "none",
//   },
//   // hide last border
//   "&:last-child td, &:last-child th": {
//     borderBottom: "none",
//   },
// }));


// const tableHead = [
//   "รหัสแท่นชาร์จ",
//   "ชื่อแท่นชาร์จ",
//   "เริ่มชาร์จ",
//   "สิ้นสุดการชาร์จ",
//   "เวลาชาร์จรวม",
//   "อัตราการใช้พลังงาน",
//   "ช่องทางชำระเงิน",
//   "จำนวนเงินรวม",
// ];

// export default function ManageHistoryTable() {
//   const [managementdata, setManagementdata] = useState([]);
//   const [counts,setCount] = useState([])

//   const { setIsLoading } = useLoading();

//   //ประวัติการชาร์จ
//   const getApi = async () => {
//     setIsLoading(true);
//     try {
//       const body = {
//         _id: "6412a9fa43091da4da5d7808",
//         PaymentMethod: "",
//         chargePointName: "",
//         StartTime: "",
//         EndTime: "",
//       };
//       const res = await apiFetch.post(
//         `/stations/mylist/detailhistory/charger?sorting=DESC&perPage=&page=`,
//         body
//       );
//       if (res.data.res_code === "0000") {
//         setManagementdata(res.data.res_data.DetailTransaction);
//         setCount(res.data.res_data);
//         console.log(res.data.res_data, "ประวัติการใช้งาน");
//       } else {
//         warningAlert(res.data.res_message);
//       }
//     } catch (error) {
//       console.log(error);
//     }
//     setIsLoading(false);
//   };

//   useEffect(() => {
//     getApi();
//   }, []);

//   return (
//     <>
//       <Box className="tableHeader">ประวัติการชาร์จ</Box>
//       <TableContainer component={Paper}>
//         <Table
//           sx={{ background: "#000000", minWidth: 700 }}
//           aria-label="customized table"
//         >
//           <TableHead>
//             <TableRow>
//               {tableHead.map((item, index) => (
//                 <StyledTableCell key={index} align="center">
//                   {item}
//                 </StyledTableCell>
//               ))}
//             </TableRow>
//           </TableHead>
//           <TableBody>
//             {managementdata.map((row, index) => (
//               <StyledTableRow key={index}>
//                 <StyledTableCell component="th" scope="row">
//                   {row.NumberCharger}
//                 </StyledTableCell>
//                 <StyledTableCell align="center">
//                   {row.chargePointName}
                  
//                 </StyledTableCell>
//                 <StyledTableCell align="center">
//                   {row.created_at}
//                 </StyledTableCell>
//                 <StyledTableCell align="center">
//                   {row.end_at}
//                 </StyledTableCell>
//                 <StyledTableCell align="center" sx={{ minWidth: "120px" }}>
//                   {row.ResultChargerTransaction.Hour}&nbsp;ชม.{counts.count}&nbsp;นาที
//                 </StyledTableCell>
//                 <StyledTableCell align="center">
//                   {row.differenceMeter}
//                 </StyledTableCell>
//                 <StyledTableCell align="center">
//                   {row.PaymentMethod}
//                 </StyledTableCell>
//                 <StyledTableCell align="center">
//                   {row.expenses}
//                 </StyledTableCell>
//               </StyledTableRow>
//             ))}
//           </TableBody>
//         </Table>
//       </TableContainer>

//       <Box sx={{ display: "flex", justifyContent: "flex-end", mt: 2 }}>
//         <Pagination
//           count={managementdata.length}
//           showFirstButton
//           showLastButton
//           variant="outlined"
//           shape="rounded"
//         />
//       </Box>
//     </>
//   );
// }

import React from 'react'

function ManageHistoryTable() {
  return (
    <div>ManageHistoryTable</div>
  )
}

export default ManageHistoryTable
