import React, { useEffect, useState } from "react";
import { getCookie, getCookies } from "cookies-next";
import Header from "../header";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import styles from "./ManagementHistory.module.scss";
import {
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Pagination,
} from "@mui/material";

import {
  Box,
  Container,
  Grid,
  Paper,
  InputBase,
  Button,
  TextField,
  FormControl,
  Select,
  MenuItem,
} from "@mui/material";

import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";

import { styled } from "@mui/material/styles";
import apiFetch from "@/helpers/interceptors";
import useLoading from "@/hooks/useLoading";
import { errorAlert } from "@/utils/alert";

// import timezone from "dayjs/plugin/timezone";
// dayjs.extend(timezone);
// dayjs.tz.setDefault("Asia/Bangkok");
// const now = dayjs(); // get the current time in "Asia/Bangkok" timezone
// const formattedTime = now.format("YYYY-MM-DD HH:mm:ss"); // format the time
// console.log(formattedTime, "formattedTime");


const TextFieldStyle = styled(TextField)(({ theme }) => ({
  "& .MuiOutlinedInput-notchedOutline": {
    border: "none",
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    //   color: theme.palette.common.white,
    color: "000000",
    backgroundColor: "#FDE1E9",
    borderBottom: "none",
    whiteSpace: "nowrap",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    backgroundColor: "#FCFCFC",
    border: 0,
    color: "#000000",
    borderBottom: "none",
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    borderBottom: "none",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    borderBottom: "none",
  },
}));

const tableHead = [
  "รหัสแท่นชาร์จ",
  "ชื่อแท่นชาร์จ",
  "เริ่มชาร์จ",
  "สิ้นสุดการชาร์จ",
  "เวลาชาร์จรวม",
  "อัตราการใช้พลังงาน",
  "ช่องทางชำระเงิน",
  "จำนวนเงินรวม",
];
{
  /* */
}

const id = getCookie("_id");
console.log(id, "id");

export default function ManagementHistory() {
  const [paramPayment, setParamPayment] = useState("");
  const [paramChager, setParamChager] = useState("");
  const [paramDateStart, setParamDateStart] = useState("");
  const [parampDateEnd, setParampDateEnd] = useState("");

  const [time, setTime] = useState({
    start: "",
    end: "",
  });

  // const [station, setStation] = useState("เลือกแท่นชาร์จที่ต้องการค้นหา");
  const [paymentSelect, setPaymentSelect] = useState([]);
  const [ChagerSelect, setChagerSelect] = useState([]);

  const { setIsLoading } = useLoading();
  const [perPage, setPerPage] = useState(6); // ขอมูลออกมาทีละ 2 ชุด
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [counts, setCounts] = useState([]);
  const [rows, setRows] = useState([]);

  const seacrh = () => {
    console.log("paramPayment", paramPayment);
    console.log("paramChager =>", paramChager);
    console.log("paramDateStart =>", JSON.stringify(time));
    console.log(
      "paramDateStart =>",
      JSON.stringify(dayjs(paramDateStart).format())
    );
    console.log("parampDateEnd =>", JSON.stringify(parampDateEnd));

    // const timeZonedata = () => {
    //   today.toLocaleString("en-US", {timeZone: "Asia/Bangkok"});
    // }
    // console.log('timeZone',timeZonedata)

    const body = {
      PaymentMethod: paramPayment,
      chargePointName: paramChager,
      StartTime: paramDateStart ? JSON.stringify(paramDateStart) : "",
      EndTime: parampDateEnd ? JSON.stringify(parampDateEnd) : "",
    };
    getApiDat(body);
  };

  const getApidropdownrecord = async () => {
    setIsLoading(true);
    try {
      const res = await apiFetch.get(
        `/stations/mylist/paymentmethod?_id=${id}`
      );

      if (res.data.res_code === "0000") {
        setPaymentSelect(res.data.res_data);
      } else {
        warningAlert(res.data.res_message);
      }
    } catch (error) {
      // errorAlert(error);
      console.log(error);
    }
    setIsLoading(false);
  };

  //เลือกแท่นชาร์จที่ต้องการค้นหา
  const getApicharging = async () => {
    setIsLoading(true);
    try {
      const res = await apiFetch.get(
        `/stations/mylist/namecharger/noobjectname/?_id=${id}`
      );
      if (res.data.res_code === "0000") {
        setChagerSelect(res.data.res_data);
        // console.log(res.data.res_data, "charging");
      } else {
        warningAlert(res.data.res_message);
      }
    } catch (error) {
      errorAlert(error);
    }
    setIsLoading(false);
  };

  //ประวัติการชาร์จ
  const getApiDat = async (val) => {
    setIsLoading(true);
    try {
      const body = {
        _id: id,
        PaymentMethod: val?.PaymentMethod || "",
        chargePointName: val?.chargePointName || "",
        StartTime: val?.StartTime || "",
        EndTime: val?.EndTime || "",
      };

      const res = await apiFetch.post(
        `/stations/mylist/detailhistory/charger?sorting=DESC&perPage=${perPage}&page=${page}`,
        body
      );

      console.log("res", res);
      if (res.data.res_code === "0000") {
        setRows(res.data.res_data.DetailTransaction);
        setCount(Math.ceil(res.data.res_data.count / perPage));
        console.log(res.data.res_data, "ประวัติการชาร์จ");
      } else {
        warningAlert(res.data.res_message);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const handleChange = (event, value) => {
    // console.log("Log-e", value);
    setPage(value);
  };

  useEffect(() => {
    getApidropdownrecord();
    getApicharging();
  }, []);

  useEffect(() => {
    // console.log("useEffect", page);
    getApiDat();
  }, [page]);

  return (
    <Box sx={{ width: "100%" }}>
      <Header title={"Charger Management ประวัติการใช้งาน"} />
      <Box className="bgSky" sx={{ minHeight: "90vh" }}>
        <Container sx={{ pt: 4 }}>
          <Grid container rowSpacing={{ xs: 2 }} columnSpacing={{ xs: 1 }}>
            <Grid item xs={12} sm={5} md={6}>
              <Box className={styles.containerBox1}>
                <FormControl fullWidth>
                  <Select
                    value={paramPayment}
                    placeholder="asdasd"
                    displayEmpty
                    onChange={(e) => setParamPayment(e.target.value)}
                    size="small"
                    sx={{
                      background: "#fff",
                      boxShadow: "none",
                    }}
                  >
                    {paymentSelect.map((item, index) => (
                      <MenuItem key={index} value={item}>
                        {item}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12} sm={7} md={6}>
              <Grid
                container
                direction="row"
                alignItems="center"
                rowSpacing={{ xs: 2 }}
                columnSpacing={{ xs: 1 }}
              >
                <Grid item xs={12} sm={5}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      inputFormat="MM/DD/YYYY"
                      value={paramDateStart}
                      onChange={(e) => setParamDateStart(e)}
                      renderInput={(params) => (
                        <TextFieldStyle
                          {...params}
                          inputProps={{
                            ...params.inputProps,
                            placeholder: "Start Date",
                          }}
                          fullWidth
                          size="small"
                          sx={{
                            background: "#fff",
                            borderRadius: "4px",
                            border: "1px solid #555555",
                          }}
                        />
                      )}
                    />
                  </LocalizationProvider>
                </Grid>

                <Grid item xs={12} sm={5}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      inputFormat="MM/DD/YYYY"
                      value={parampDateEnd}
                      onChange={(e) => setParampDateEnd(e)}
                      renderInput={(params) => (
                        <TextFieldStyle
                          {...params}
                          inputProps={{
                            ...params.inputProps,
                            placeholder: "End Date",
                          }}
                          fullWidth
                          size="small"
                          sx={{
                            background: "#fff",
                            borderRadius: "4px",
                            border: "1px solid #555555",
                          }}
                        />
                      )}
                    />
                  </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={2}>
                  <Button
                    className="btnBlue"
                    fullWidth
                    onClick={() => seacrh()}
                  >
                    ค้นหา
                  </Button>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12} sm={5} md={6}>
              <Box className={styles.containerBox1}>
                <FormControl fullWidth>
                  <Select
                    value={paramChager}
                    displayEmpty
                    onChange={(e) => setParamChager(e.target.value)}
                    size="small"
                    sx={{
                      background: "#fff",
                      boxShadow: "none",
                    }}
                  >
                    {ChagerSelect.map((item, index) => (
                      <MenuItem key={index} value={item}>
                        {item}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
            </Grid>
          </Grid>

          <Box sx={{ mt: 3 }}>
            {/* <ManageHistoryTable />*/}

            <Box className="tableHeader">ประวัติการชาร์จ</Box>
            <TableContainer component={Paper}>
              <Table
                sx={{ background: "#000000", minWidth: 700 }}
                aria-label="customized table"
              >
                <TableHead>
                  <TableRow>
                    {tableHead.map((item, index) => (
                      <StyledTableCell key={index} align="center">
                        {item}
                      </StyledTableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map((row, index) => (
                    <StyledTableRow key={index}>
                      <StyledTableCell component="th" scope="row">
                        {row.NumberCharger}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.chargePointName}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {/*moment(post.date).format() */}
                        {row.created_at}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.end_at}
                      </StyledTableCell>
                      <StyledTableCell
                        align="center"
                        sx={{ minWidth: "120px" }}
                      >
                        {row.ResultChargerTransaction.Hour}&nbsp;ชม.
                        {counts.count}&nbsp;นาที
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.differenceMeter}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.PaymentMethod}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.expenses}&nbsp;บาท
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>

            {/*count*/}

            <Box sx={{ display: "flex", justifyContent: "flex-end", mt: 2 }}>
              <Pagination
                count={count}
                showFirstButton
                showLastButton
                variant="outlined"
                shape="rounded"
                page={page}
                onChange={handleChange}
              />
            </Box>
          </Box>
        </Container>
      </Box>
    </Box>
  );
}
