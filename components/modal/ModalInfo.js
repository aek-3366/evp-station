import React from "react";
import styles from "./Modal.module.scss";
import {
  Box,
  Modal,
  Typography,
  TextField,
  Button,
  Container,
  FormControl,
  Select,
  MenuItem,
} from "@mui/material";
import { styled } from "@mui/material/styles";

const TextFieldStyle = styled(TextField)(({ theme }) => ({
  "& .MuiOutlinedInput-root": {
    borderRadius: "10px",
    border: "1px solid #999999",
  },
  "& .MuiOutlinedInput-notchedOutline": {
    border: "0",
  },
}));

const SelectStyle = styled(Select)(({ themt }) => ({
  "& .MuiOutlinedInput-notchedOutline": {
    borderColor: "#999999 !important",
  },
}));


export default function ModalInfo({ openModal, handleCloseModal, dataInmodal }) {
  return (
    <Box>
      <Modal
        open={openModal}
        onClose={handleCloseModal}
        className={styles.modalBgColor}
      >
        <Box className={styles.modalBox} sx={{ width: "400px" }}>
          <Box className="tableHeader" sx={{ borderRadius: "10px 10px 0 0" }}>
            สถานที่ตั้ง
          </Box>
          <Container sx={{ py: 2, background: "F2FBF6" }}>
            <Box>
              <Typography>ที่อยู่</Typography>
              <TextFieldStyle
                fullWidth
                size="small"
                defaultValue={dataInmodal.DetailAddress?.Address}
                placeholder="ที่อยู่"
                disabled
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>จังหวัด</Typography>
              <TextFieldStyle
                fullWidth
                size="small"
                placeholder="จังหวัด"
                defaultValue={dataInmodal.DetailAddress?.Province}
                disabled
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>อำเภอ/เขต</Typography>
              <TextFieldStyle
                fullWidth
                size="small"
                placeholder="อำเภอ/เขต"
                defaultValue={dataInmodal.DetailAddress?.District}
                disabled
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>ตำบล/แขวง</Typography>
              <TextFieldStyle
                fullWidth
                defaultValue={dataInmodal.DetailAddress?.Parish}
                size="small"
                placeholder="ตำบล/แขวง"
                disabled
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>เลขไปรษณีย์</Typography>
              <TextFieldStyle
                fullWidth
                size="small"
                defaultValue={dataInmodal.DetailAddress?.PostalNumber}

                placeholder="เลขไปรษณีย์"
                disabled
              />
            </Box>

            <Box sx={{ display: "flex", justifyContent: "end", mt: 2 }}>
              <Button
                className="btnCream"
                sx={{ width: "140px" }}
                onClick={() => handleCloseModal()}
              >
                ปิด
              </Button>
            </Box>
          </Container>
        </Box>
      </Modal>
    </Box>
  );
}
