import React, { useState, useEffect, useContext } from "react";
import { Box, Button, Container, Modal, Typography } from "@mui/material";
import styles from "./Modal.module.scss";
import { useRouter } from "next/router";
import apiFetch from "@/helpers/interceptors";
import useLoading from "@/hooks/useLoading";
import AuthContext from "@/context/AuthProvider";
import { errorAlert, successAlert, warningAlert } from "@/utils/alert";
import { setCookie, deleteCookie } from "cookies-next";

export default function ModalChooseStation({
  openModal,
  handleCloseModal,
  station,
}) {
  const [chooseStation, setChooseStation] = useState(null);
  const [stationRows, setStationRows] = useState([]);
  const { setIsLoading } = useLoading();
  const context = useContext(AuthContext);
  const router = useRouter();

  const handleSelect = (id) => {
    if (chooseStation === id) {
      setChooseStation(null);
    } else {
      setChooseStation(id);
    }
  };

  const goPage = async () => {
    //   ให้เลือกสถานีก่อน ถึง เปลี่ยน route ไปหน้าอื่น
    if (!chooseStation) {
      return;
    }
    setIsLoading(true);
    try {
      const body = {
        _id: chooseStation,
      };
      const res = await apiFetch.post(`/stations/mylist/detailstation`, body);

      if (res.data.res_code === "0000") {
        setCookie("_id", chooseStation);
        context.setIdStation(chooseStation);
        router.replace("/management/t?type=management-list");
      } else {
        warningAlert(res.data.res_message);
        deleteCookie("accInfo");
        deleteCookie("_id");
        deleteCookie("token");
        handleCloseModal()
        router.replace("/");
      }
    } catch (error) {
      errorAlert(error);
      deleteCookie("accInfo");
      deleteCookie("_id");
      deleteCookie("token");
      handleCloseModal()
      router.replace("/");
    }
    setIsLoading(false);
  };

  useEffect(() => {
    setStationRows(station);
  }, [station]);

  return (
    <Box>
      <Modal
        open={openModal}
        onClose={handleCloseModal}
        className={styles.modalBgColor}
      >
        <Box className={styles.modalBox} sx={{ width: "400px" }}>
          <Box className="tableHeader" sx={{ borderRadius: "10px 10px 0 0" }}>
            เลือกสถานที่ดูแล
          </Box>
          <Container sx={{ py: 2, background: "F2FBF6" }}>
            {stationRows.map((item, index) => (
              <Box
                key={index}
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  border: "1px solid #555555",
                  color: "#999999",
                  borderRadius: "10px",
                  cursor: "pointer",
                  mb: 2,
                  p: 1,
                }}
                className={chooseStation === item._id ? styles.selectAtive : ""}
                onClick={() => handleSelect(item._id)}
              >
                <Typography>สถานที่</Typography>
                <Typography>{item.NAME}</Typography>
              </Box>
            ))}

            <Button
              className={chooseStation === null ? "btnGray2" : "btnBlack"}
              fullWidth
              sx={{ borderRadius: "10px", mt: 10 }}
              style={{ cursor: chooseStation !== null ? "pointer" : "no-drop" }}
              onClick={() => goPage()}
            >
              ยืนยัน
            </Button>
          </Container>
        </Box>
      </Modal>
    </Box>
  );
}
