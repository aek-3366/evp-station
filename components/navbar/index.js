import React, { useState } from "react";
import styles from "./Navbar.module.scss";
import { Box, IconButton, Menu, MenuItem, Typography } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import Image from "next/image";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { useRouter } from "next/router";
import { deleteCookie } from "cookies-next";

export default function Navbar() {
  const router = useRouter();
  const [anchorEl, setAnchorEl] = useState(null);
  const [openMenu, setOpenMenu] = useState(false);

  const [anchorElSub, setAnchorElSub] = useState({
    charger: null,
    bill: null,
  });

  const [openMenuSub, setOpenMenuSub] = useState({
    charger: false,
    bill: false,
  });

  const handleClick = (event) => {
    setOpenMenu(true);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setOpenMenu(false);
    setAnchorEl(null);
  };

  const handleClickSubMenu = (event, status) => {
    switch (status) {
      case "charger":
        setOpenMenuSub({ ...openMenuSub, charger: true });
        setAnchorElSub({ ...anchorElSub, charger: event.currentTarget });
        break;
      case "bill":
        setOpenMenuSub({ ...openMenuSub, bill: true });
        setAnchorElSub({ ...anchorElSub, bill: event.currentTarget });
        break;

      default:
        setOpenMenuSub({ ...openMenuSub, charger: true, bill: true });
        setAnchorElSub({
          ...anchorElSub,
          charger: event.currentTarget,
          bill: event.currentTarget,
        });
        break;
    }
  };

  const handleCloseSubMenu = () => {
    const bodyAnchorEl = {
      charger: null,
      bill: null,
    };
    const bodyOpenMenu = {
      charger: false,
      bill: false,
    };
    setAnchorElSub(bodyAnchorEl);
    setOpenMenuSub(bodyOpenMenu);
    setOpenMenu(false);
    setAnchorEl(null);
  };

  const logout = () => {
    deleteCookie("accInfo");
    deleteCookie("_id");
    deleteCookie("token");
    router.replace("/");
  };

  return (
    <Box className={styles.containerNavbar}>
      <IconButton sx={{ color: "#fff", mr: 1 }} onClick={(e) => handleClick(e)}>
        <MenuIcon sx={{ fontSize: 36 }} />
      </IconButton>

      <Menu anchorEl={anchorEl} open={openMenu} onClose={() => handleClose()}>
        <MenuItem
          className={
            router.pathname.includes("management")
              ? styles.subMenuActive
              : styles.subMenuNotActive
          }
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
            onClick={(e) => handleClickSubMenu(e, "charger")}
          >
            <Typography
              className={`${
                router.pathname.includes("management") && styles.textBlue
              }`}
            >
              Charger Management
            </Typography>
            <KeyboardArrowDownIcon className="textDark" />
          </Box>
          <Menu
            anchorEl={anchorElSub.charger}
            open={openMenuSub.charger}
            onClose={() => handleCloseSubMenu()}
          >
            <MenuItem
              onClick={() => {
                router.push("/management/t?type=management-list");
                handleCloseSubMenu();
              }}
              className={
                router.query.type === "management-list"
                  ? styles.subMenuActive
                  : styles.subMenuNotActive
              }
            >
              รายการแท่นชาร์จ
            </MenuItem>
            <MenuItem
              onClick={() => {
                router.push("/management/t?type=management-history");
                handleCloseSubMenu();
              }}
              className={
                router.query.type === "management-history"
                  ? styles.subMenuActive
                  : styles.subMenuNotActive
              }
            >
              ประวัติการใช้งาน
            </MenuItem>
            <MenuItem
              onClick={() => {
                router.push("/management/t?type=management-infomation");
                handleCloseSubMenu();
              }}
              className={
                router.query.type === "management-infomation"
                  ? styles.subMenuActive
                  : styles.subMenuNotActive
              }
            >
              ข้อมูลแท่นชาร์จ
            </MenuItem>
          </Menu>
        </MenuItem>

        <MenuItem
          className={
            router.pathname.includes("bill")
              ? styles.subMenuActive
              : styles.subMenuNotActive
          }
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
            onClick={(e) => handleClickSubMenu(e, "bill")}
          >
            <Typography
              className={`${
                router.pathname.includes("bill") && styles.textBlue
              }`}
            >
              Bill
            </Typography>
            <KeyboardArrowDownIcon className="textDark" />
          </Box>
          <Menu
            anchorEl={anchorElSub.bill}
            open={openMenuSub.bill}
            onClose={() => handleCloseSubMenu()}
          >
            <MenuItem
              onClick={() => {
                router.push("/bill/t?type=bill-history");
                handleCloseSubMenu();
              }}
              className={
                router.query.type === "bill-history"
                  ? styles.subMenuActive
                  : styles.subMenuNotActive
              }
            >
              ประวัติการจ่ายบิล
            </MenuItem>
            <MenuItem
              onClick={() => {
                router.push("/bill/t?type=bill-revenue");
                handleCloseSubMenu();
              }}
              className={
                router.query.type === "bill-revenue"
                  ? styles.subMenuActive
                  : styles.subMenuNotActive
              }
            >
              รายได้ของร้าน
            </MenuItem>
          </Menu>
        </MenuItem>

        <MenuItem>
          <Typography onClick={() => logout()}>ออกจากระบบ</Typography>
        </MenuItem>
      </Menu>

      <Box sx={{ flexGrow: 1 }}>
        <Image
          className={styles.imgLogo}
          src="/images/logo/logo-login.png"
          alt=""
          width="100"
          height="0"
        />
      </Box>
    </Box>
  );
}
